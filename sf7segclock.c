/*************************************************************************
 *
 * sf7segclock.c - a daemon to drive the SparkFun Electronics 7-Segment
 * 	serial display as a 12 or 24 hour digital clock
 *
 * copyright Robert Edwards, 2010
 *
 * licensed under the GPLv2
 */

#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/file.h>
#include <signal.h>
#include <errno.h>
#include "misclib.h"

#define BAUD 9600
#define DEF_PORT "/dev/ttyUSB0"
#define HR12_FORMAT "%I%M"
#define HR24_FORMAT "%H%M"
#define OPT_STR "p:tvz:"

#define MAX_BUF 99

static int debugLvl = 0;

int done = 0;

void term_handler (int s) {
	done = 1;
} /* term_handler () */

void usage (char *prog_name, char *msg) {
	if (msg) fprintf (stderr, "%s\n", msg);
	fprintf (stderr, "usage : %s [-p ser_dev] [-t] [-z timezone]\n", prog_name);
	exit (0);
} /* usage () */

int main (int argc, char *argv[]) {
	char *prog_name;
	char c;
	char *serport = DEF_PORT;
	char *format = HR24_FORMAT;
	int serfd;
	struct sigaction act;

	prog_name = basename (strdup (argv[0]));

	while ((c = getopt (argc, argv, OPT_STR)) >= 0) {
		switch (c) {
			case 'p': serport = strdup (optarg); break;
			case 't': format = HR12_FORMAT; break;
			case 'v': debugLvl++; break;
			case 'z': setenv ("TZ", optarg, 1); break;
			default: usage (prog_name, "unknown option"); break;
		}
	}

	serfd = serialSetup (serport, BAUD, 0, 0);
	if (serfd <= 0) {
		int tmp_err = errno;
		perror ("serialSetup");
		fprintf (stderr, "unable to open serial port %s ", serport);
		if (tmp_err == EWOULDBLOCK) {
			fprintf (stderr, "port already in use?\n");
		} else {
			fprintf (stderr, "try specifying one with -p option\n");
		}
		exit (0);
	}
	if (debugLvl > 0) {
		printf ("serial FD is %d\n", serfd);
	}

	if ((write (serfd, "xxxx", 4) == -1) ||	// clear display
		(write (serfd, "w0", 2) == -1)) {		// turn on colon
		perror ("write");
		exit (0);
	}

	if (!debugLvl) {
		daemonize ();
	}

	memset (&act, 0, sizeof(act));
	act.sa_handler = term_handler;
	if (sigaction (SIGTERM, &act, NULL) == -1) {
		perror ("sigaction");
	}

	while (!done) {
		char buf [MAX_BUF + 1];
		time_t t;
		struct tm *tmp;

		t = time (NULL);
		tmp = localtime (&t);
		if (tmp) {
			if (strftime (buf, MAX_BUF, format, tmp) == 4) {
				if (buf[0] == '0') buf[0] = 'x';
				write (serfd, buf, 4);
				printf ("%s\n", buf);
			}
		}
		sleep (60 - tmp->tm_sec);
	}
	flock (serfd, LOCK_UN);
	close (serfd);
	return 0;
} /* main () */
