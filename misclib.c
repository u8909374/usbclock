/*
 * misc.c - miscellaneous routines
 *
 * Copyright 2006, 2007, 2008, 2009 Robert Edwards, Australian National University
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "misclib.h"

int daemonize (void) {
	pid_t pid;

	if ((pid = fork ()) < 0) return -1;
	if (pid != 0) exit (0);	/* get rid of parent */
	/* now in child */
	setsid ();		/* become session leader */
	chdir ("/");
	umask (0);
	close (0);
	close (1);
	close (2);
	return 0;
} /* daemonize */

void sigChild (int sig) {
	while (1) {
		if (wait (NULL) == -1) {
			if (errno != EINTR) return;
		} else return;
	}
} /* sigChild () */

void strAppend (char *buf, char *join, char *new) {
	if (strlen (buf) > 0) strcat (buf, join);
	strcat (buf, new);
} /* strAppend () */

int hex2num (const char in) {
	if ((in >= '0') && (in <= '9')) return (in - '0');
	if ((in >= 'A') && (in <= 'F')) return (in - 'A' + 10);
	if ((in >= 'a') && (in <= 'f')) return (in - 'a' + 10);
	return -1;
} /* hex2num () */

char num2hex (const int in) {
	if (in < 0) return '\0';
	if (in <= 9) return in + '0';
	if (in <= 15) return in + 'a' - 10;
	return '\0';
} /* num2hex () */

int strhex2bin (const char *in, unsigned char *out) {
	int num_out = 0;
	int msn = 0;
	int lsn;
	int flag;

	flag = (strlen (in) & 1);
	while (*in) {
    		if ((lsn = hex2num (*in++)) == -1) return -1;
		if (flag) {
			*out++ = msn* 16 + lsn;
			num_out++;
		} else {
			msn = lsn;
		}
		flag = !flag;
	}
	return (num_out);
} /* strhex2bin () */

void hex_encode (char *dst, const unsigned char *src, size_t srcSize) {
	while (srcSize--) {
		*dst++ = num2hex ((*src >> 4) & 0xf);
		*dst++ = num2hex (*src++ & 0xf);
	}
	*dst = '\0';
}

#ifndef MAX_STR
#define MAX_STR 255
#endif
/*
 * take a string (typically from a config file) and strip off whitespace
 * if the string then starts with double quotes, return everything up to
 * closing double quotes, except for single-quoted and escaped double quotes.
 */
char *quoted_str (char *cp) {
	char *start;
	char *out;
	char contexts[MAX_STR + 1];
	char *ctx;
	int finished = 0;

	if (!cp || !*cp) return cp;				/* check for empty string */
	if (strlen (cp) > MAX_STR) return (cp + strlen (cp));
	while (!isgraph (*cp)) cp++;			/* remove leading whitespace */
	contexts[MAX_STR] = 0;					/* null terminate... */
	ctx = contexts;
	*ctx = ' ';
	start = out = cp;
	if (*cp == '"') {
		*++ctx = *cp++;
		start = out = cp;
	}
	while (*cp && !finished) {
		*out++ = *cp;
		switch (*ctx) {
			case '"': switch (*cp) {
					case '\\': *++ctx = *cp; out--; break;
					case '"': finished = (*--ctx == ' '); break;
					case '\'': *++ctx = *cp; break;
					default: break;
				}; break;
			case '\\': ctx--; break;
			case '\'': if (*cp == '\'') ctx--; break;
			default: finished = !isgraph (*cp); break;
		}
		cp++;
	}
	if (finished) out--;			/* back up the output pointer */
	if (finished || (*ctx == ' ')) {
		*out = 0;
		return (start);
	}
	return (cp);
} /* quoted_str () */

/*
 * take a line of char (typically read from a configuration file) and
 * find the keyword/value tuple, separated by space, tab, equals and/or colon
 * return 1 for OK or 0 if line is blank, starts with a comment (#) or
 * does not have a keyword containing only alphanumerics and underscore
 */
int config_parser (char *cp, char **key, char **val) {
	cp[strlen (cp) - 1] = 0;
	*key = cp;
	*val = cp + strlen (cp);			/* start it off as null string */
	while ((*cp == ' ') || (*cp == '\t')) cp++;		/* strip whitespace */
	if (strlen (cp) == 0) return 0;					/* skip blank lines */
	if (*cp == '#') return 0;		/* skip lines starting with comment */
	*key = cp;
	while (isalnum (*cp) || (*cp == '_')) cp++; 	/* find end of key */
	if (isgraph (*cp) && (*cp != '=') && (*cp != ':')) return 0;
	*cp++ = 0;
	while (!isgraph (*cp) || (*cp == '=') || (*cp == ':')) cp++;
	*val = quoted_str (cp);
	for (cp = *key; *cp; cp++) *cp = tolower (*cp);	/* make key lowercase */
	return 1;
} /* config_parser () */

#define MAX_CONF_LEN 255
int config_load (char *filename, struct conftokens *confinfo) {
	FILE *fp;
	char line[MAX_CONF_LEN + 1];
	char *token;
	char *value;
	int i;
    
	fp = fopen (filename, "r");
	if (!fp) {	
		perror ("no config file");
		return 0;
	}

	while (fgets (line, MAX_CONF_LEN, fp)) {
		if ((line[0]=='#')) continue;	/* skip lines with comment */
		line[strlen (line) - 1] = 0; /* kill '\n' */
		token = strtok_r (line, "=", &value);
		token = strtok (token, " \t");
		if (token != NULL) {
			for (i = 0; (confinfo[i].token != NULL) && (strcasecmp (token, confinfo[i].token)); i++); 
			if (confinfo[i].token != NULL) {
				while ((value != NULL) && (isspace ((int) *value))) value++;
				value = strtok (value, " #\t");
				confinfo[i].value = strdup (value);
			}
		}
	}
	fclose(fp);
	return 1;
} /* config_load () */

void config_print (struct conftokens *confinfo) {
	int i;

	for (i = 0; confinfo[i].token != NULL; i++) {
		if (confinfo[i].value) {
			syslog (LOG_DEBUG, "%s = %s\n", confinfo[i].token, confinfo[i].value);
		} else {
			syslog (LOG_DEBUG, "%s", confinfo[i].token);
		}
	}
} /* config_print () */

int serialSetup (char *dev, int baud, int raw, int hshk) {
	speed_t speed;
	int fd;
	struct termios current;
	struct termios options;
	
	switch (baud) {
		case 600: speed = B600; break;
		case 1200: speed = B1200; break;
		case 2400: speed = B2400; break;
		case 4800: speed = B4800; break;
		case 9600: speed = B9600; break;
		case 19200: speed = B19200; break;
		case 38400: speed = B38400; break;
		case 57600: speed = B57600; break;
		case 115200: speed = B115200; break;
		case 230400: speed = B230400; break;
		default: return -1;
	}

	fd = open (dev, O_RDWR | O_NOCTTY);
	if (fd < 0) {
		return fd;
	}
	if (flock (fd, LOCK_EX | LOCK_NB) < 0) {
		close (fd);
		return -1;
	}
	if (tcgetattr (fd, &current) < 0) {
		close (fd);
		return (-1);
	}
	memcpy (&options, &current, sizeof (options));
	cfsetispeed (&options, speed);
	cfsetospeed (&options, speed);
	if (raw) {
		cfmakeraw (&options);
	}
	// disable all flow control
	options.c_iflag &= ~(IXON | IXOFF);
	options.c_oflag &= ~CRTSCTS;
	switch (hshk) {
		case SER_HANDSHAKE_HW:
			options.c_oflag |= CRTSCTS; break;
		case SER_HANDSHAKE_XON:
			options.c_iflag |= (IXON | IXOFF); break;
		default: break;
	}
	/* only attempt to set the attributes if they have changed */
	if (memcmp (&current, &options, sizeof (options))) {
		tcsetattr (fd, TCSANOW, &options);
		syslog (LOG_INFO, "changing serial port settings");
	}
	tcflush (fd, TCIOFLUSH);
	return fd;
} /* serialSetup () */

char *getsNoEcho (int fd, char *str, int size) {
	struct termios current;
	struct termios options;
	FILE *file;
	char *retval = NULL;

	if (tcgetattr (fd, &current) == -1)
		return NULL;
	options = current;
	options.c_lflag &= ~ECHO;
	if (tcsetattr (fd, TCSANOW, &options) == -1)
		return NULL;
	if ((file = fdopen (fd, "r")))
		retval = fgets (str, size, file);
	tcsetattr (fd, TCSANOW, &current);
	return retval;
} /* getsNoEcho () */

/*
 * return a new buffer with a full path filename. If name starts with /
 * then new buffer has name, otherwise prepend dir and a "/"
 */
char *prependDir (char *name, char *dir) {
	char *ret;

	if (name[0] == '/') return (strdup (name));
	ret = malloc (strlen (name) + strlen (dir) + 2);
	if (ret) sprintf (ret, "%s/%s", dir, name);
	return ret;
} /* prependDir () */

