Experiments in building a USB-powered and syncronised clock

1) a small digital clock using the Sparkfun Electronics 7-Segment Serial Display (COM-09230) purchased from Littlebird Electronics in Sydney, Australia.

Connected 3 wires to an FTDI Serial cable (which also provides +5v) and was able to start displaying time almost immediately. I was initially skeptical about this approach as the FTDI232C datasheet specifies the max current draw as 75mA. Still, it all works!

Use sf7segclock to drive this combination.

2) also works with a USB ACM-connected clock, such as USBClockit:

sf7segclock -p /dev/ttyACM0


