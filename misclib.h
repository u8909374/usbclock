/*
 * misc.h - header for miscellaneous routines
 *
 * by Robert Edwards, April 2006
 */

#ifndef _MISCLIBH
#define _MISCLIBH

#define SER_HANDSHAKE_NONE 0
#define SER_HANDSHAKE_HW 1
#define SER_HANDSHAKE_XON 2

struct conftokens {
	char *token;
	char *value;
};

int daemonize (void);
void sigChild (int sig);
void strAppend (char *buf, char *join, char *new);
int hex2num (const char in);
char num2hex (const int in);
int strhex2bin (const char *in, unsigned char *out);
void hex_encode (char *dst, const unsigned char *src, size_t srcSize);
char *quoted_str (char *cp);
int config_parser (char *cp, char **key, char **val);
int config_load (char *filename, struct conftokens *confinfo);
void config_print (struct conftokens *confinfo);
int serialSetup (char *dev, int baud, int raw, int hshk);
char *getsNoEcho (int fd, char *str, int size);
char *prependDir (char *name, char *dir);

#endif /* _MISCLIBH */
