# Makefile for usbClocks
# by Robert Edwards, April 2010

CC = gcc
LIBS = misclib.o

sf7segclock: sf7segclock.c $(LIBS)
	$(CC) -Wall -o $@ $< $(LIBS)

clean:
	rm *.o
